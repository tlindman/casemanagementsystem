package caseManagement;

import java.util.ArrayList;
import java.util.Observable;

import databaseRepository.SQLValuePair;

public abstract class Model extends Observable {
	public int id;
	
	@SuppressWarnings("rawtypes")
	public abstract ArrayList<SQLValuePair> getProperties();
	
	public int getId() {
		return this.id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
