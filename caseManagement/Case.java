package caseManagement;

import databaseRepository.CaseRepository;
import databaseRepository.SQLValuePair;
import java.util.ArrayList;

public class Case extends Model {

    //private int case_id;
    //private int id;
    private String description;
    private String report;
    private String totalCost;
    private String totalHours;
    private String actualCost;
    private String actualHours;
    private boolean isCostApproved = false;// adding default value to false?
    private int stage;
    private String author;
    private String assignedTechnician;

    public Case(String description, String report, String totalCost,String totalHours,String actualCost,
            String actualHours,boolean isCostApproved, int stage, String author, String assignedTechnician) {
        //this.case_id = case_id;
        this.description = description;
        this.report = report;
        this.totalCost = totalCost;
        this.totalHours = totalHours;
        this.actualCost = actualCost;
        this.actualHours = actualHours;
        this.isCostApproved = isCostApproved;
        this.stage = stage;
        this.author = author;
        this.assignedTechnician = assignedTechnician;
    }

    @SuppressWarnings({"rawtypes"})
    public ArrayList<SQLValuePair> getProperties() {
        ArrayList<SQLValuePair> properties = new ArrayList<SQLValuePair>();
        properties.add(new SQLValuePair<String, String>("description", this.description));
        properties.add(new SQLValuePair<String, String>("report", this.report));
        properties.add(new SQLValuePair<String, String>("totalcost", this.totalCost));
        properties.add(new SQLValuePair<String, String>("totalhours", this.totalHours));
        properties.add(new SQLValuePair<String, String>("actualcost", this.actualCost));
        properties.add(new SQLValuePair<String, String>("actualhours", this.actualHours));
        properties.add(new SQLValuePair<String, Boolean>("isCostApproved", this.isCostApproved));
        properties.add(new SQLValuePair<String, Integer>("stage", this.stage));
        properties.add(new SQLValuePair<String, String>("author", this.author));
        properties.add(new SQLValuePair<String, String>("assignedTechnician", this.assignedTechnician));
        return properties;
    }

    public String getDescription() {
        return this.description;
    }

    public String getAuthor() {
        return this.author;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }
    
    public String getTotalCost() {
        return this.totalCost;
    }
    
    public void setTotalHours(String totalHours) {
        this.totalHours = totalHours;
    }
    public String getTotalHours() {
        return this.totalHours;
    }
    
    public String getActualCost() {
        return this.actualCost;
    }
    
    public String getActualHours() {
        return this.actualHours;
    }
    
    public void setAssignedTechnician(String assignedTechnician)
    {
        this.assignedTechnician = assignedTechnician;
    }
    
    public String getAssignedTechnician()
    {
        return this.assignedTechnician;
    }

    public void setIsCostApproved(boolean b) {
        this.isCostApproved = b;
    }

    public boolean getIsCostApproved() {
        return this.isCostApproved;
    }
    
    public int getStage() {
        return this.stage;
    }
    
    public void setStage(int nr) {
        this.stage = nr;
    }

    public void SaveOfficerInput(String tech, String cost, String time) {
        System.out.println("Model:     : Sparar handläggarens input");
        this.setAssignedTechnician(tech);
        this.setTotalCost(cost);
        this.setTotalHours(time);
        this.setStage(2);
        CaseRepository caseRepo = new CaseRepository();
        caseRepo.Save(this);
        setChanged();
        notifyObservers();
    }
    public void SaveManagerInput(boolean b) {
        System.out.println("Model     : Sparar managerns input(attestering av kostnad)");
        this.setIsCostApproved(b);
        this.setStage(4);
        CaseRepository caseRepo = new CaseRepository();
        caseRepo.Save(this);
        setChanged();
        notifyObservers();
    }

    @Override
    public String toString() {
        String text = "Description: " + this.getDescription() + "\n\n";
        if(getAssignedTechnician() == null)
            text = text + "A technician has not been appointed.\n\n";
        else
            text = text + "Appointed technician is " + getAssignedTechnician() + ".\n\n";
        if (this.getTotalCost() == null) {
            text = text + "The cost and time for this case has not been set yet.\n\n";
        } else {
            text = text + "The estimated cost and time for this case is " + this.getTotalCost() + " kr " +
                    "and " + this.getTotalHours() + " hours\n\n";
        }
        if (this.getStage()== 3 || this.getStage() == 4) {
            int exceedCost = Integer.parseInt(this.getActualCost()) - Integer.parseInt(this.getTotalCost());
            text = text + "The cost for this case has been exceeded with " + exceedCost + " kr.\n\n";
            if(this.getIsCostApproved())
                text = text + "This extra cost has been approved.";
            else
                text = text + "This extra cost has NOT been approved.";
        }
        return text;
    }
}
