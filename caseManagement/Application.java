package caseManagement;

import java.util.Observable;

public class Application extends Observable {
	
	/*
	 * Purpose:
	 * Toggle variables and notify Observers that changes as been made.
	 */
	
	private boolean userIsLoggedIn = false;
	
	private static User activeUser;

	public void loginUser(User activeUser) {
		System.out.println("ApplicationModel: loginUser was called");
		this.setActiveUser(activeUser);
		this.userIsLoggedIn = true;
		this.setChanged();
		this.notifyObservers();
	}
	
	public void logout() {
		System.out.println("ApplicationModel: logout was called");
		this.setActiveUser(null);
		this.userIsLoggedIn = false;
		this.setChanged();
		this.notifyObservers();
	}

	public boolean userIsLoggedIn() {
		return userIsLoggedIn;
	}

	public static User getActiveUser() {
		return activeUser;
	}

	public void setActiveUser(User newUser) {
		activeUser = newUser;
	}
}
