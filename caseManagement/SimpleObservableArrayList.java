package caseManagement;

import java.util.ArrayList;
import java.util.Collection;

public class SimpleObservableArrayList<E> extends ArrayList<E> {

	private static final long serialVersionUID = 1L;
	private ArrayList<SimpleArrayListObserver> observers = new ArrayList<SimpleArrayListObserver>();

	/* Handle Observers */
	
	public void addObserver ( SimpleArrayListObserver observer ) {
		this.observers.add(observer);
	}
	
	public void removeObserver ( SimpleArrayListObserver observer ) {
		this.observers.remove(observer);
	}
	public void notifyObservers () {
		for( SimpleArrayListObserver o : getObservers() ) {
			o.update(this, null);
		}
	}
	private ArrayList<SimpleArrayListObserver> getObservers() {
		return this.observers;
	}
	
	/* Notify all observers about the following actions */
	
	@Override
	public boolean add(E e) {
		boolean result = super.add(e);
	
		for( SimpleArrayListObserver o : getObservers() ) {
			o.update(this, null);
		}
		return result;
	}

	@Override
	public void add(int index, E element) {
		super.add(index, element);
		
		for( SimpleArrayListObserver o : getObservers() ) {
			o.update(this, null);
		}
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		boolean result =  super.addAll(c);
		
		for( SimpleArrayListObserver o : getObservers() ) {
			o.update(this, null);
		}
		return result;
	}

	@Override
	public boolean addAll(int index, Collection<? extends E> c) {
		boolean result = super.addAll(index, c);
		
		for( SimpleArrayListObserver o : getObservers() ) {
			o.update(this, null);
		}
		return result;
		
	}

	@Override
	public void clear() {
		super.clear();
		
		for( SimpleArrayListObserver o : getObservers() ) {
			o.update(this, null);
		}
	}

	@Override
	public E remove(int index) {
		E result = super.remove(index);
		
		for( SimpleArrayListObserver o : getObservers() ) {
			o.update(this, null);
		}
		return result;
	}

	@Override
	public boolean remove(Object obj) {
		boolean result = super.remove(obj);
		
		for( SimpleArrayListObserver o : getObservers() ) {
			o.update(this, null);
		}
		return result;
	}

	@Override
	public E set(int index, E element) {
		
		E result = super.set(index, element);
		
		for( SimpleArrayListObserver o : getObservers() ) {
			o.update(this, null);
		}
		return result;
	}
	
}
