package caseManagement;

import java.util.ArrayList;

import databaseRepository.CaseRepository;
import databaseRepository.SQLValuePair;
import databaseRepository.UserRepository;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

public class ApplicationController{
	
	ApplicationView view;
	Application model = new Application();
	UserRepository userRepository = new UserRepository();
	CaseRepository caseRepository = new CaseRepository();
	
	ArrayList<SQLValuePair<?,?>> conditions = new ArrayList<SQLValuePair<?,?>>();	
	
	public ApplicationController(ApplicationView view, Application model) {
		this.model = model;
		model.addObserver(view);
		this.view = view;
		
		System.out.println("ApplicationController was created");
	}
	public void login(String username, String password) {
		System.out.println("ApplicationController: login was called");
		
		/*
		 * Checks if a user with given username and password exists
		 * then calls the .loginUser() method on the ApplicationModel
		 */
		ArrayList<SQLValuePair<String, ?>> conditions = new ArrayList<SQLValuePair<String, ?>>();
		
		conditions.add(new SQLValuePair<String, String>("username", username));
		conditions.add(new SQLValuePair<String, String>("password", password));
		
		User user = userRepository.FindOne(conditions);
		
		if(user != null) {
                    if(user.getRole().equals("Manager") || user.getRole().equals("Officer"))
			model.loginUser(user);
                    else JOptionPane.showMessageDialog(null, "You have no authority!!!");
		}
		else {
			System.out.println("ApplicationController: No user was found");
		}
	}
	
	public void logout() {
	System.out.println("ApplicationController: logout was called");
		model.logout();
	}
}
    