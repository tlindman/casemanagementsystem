package caseManagement;

import javax.swing.JTable;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import databaseRepository.SQLValuePair;

import javax.swing.JLabel;
import java.awt.Font;

public class CasesView extends JPanel implements SimpleArrayListObserver, MouseListener{
	
	public CasesController controller;
	private SimpleObservableArrayList<Case> modelArray; // Is set later by this views update method.
	
	private static final long serialVersionUID = -1689553421801002784L;
	
	private Object columnNames[] = { "Report ID", "Written By"};
	private Object rowData[][] = {};
	
	private DefaultTableModel tableModel = new DefaultTableModel(rowData, columnNames);
	private JTable table; 
	private JLabel viewLabel;
	
	private void setRows() {
		this.tableModel.setRowCount(0);
		
		for( Case c : modelArray ) {
			tableModel.addRow(new Object[]{c.getId(),c.getAuthor()});
		}
	}
	
	public CasesView(String vName, ArrayList<SQLValuePair<String,?>> modelFilter) {
		
		table = new JTable(rowData, columnNames);	
		table.setModel(tableModel);
	   
	    setLayout(null);
	    JScrollPane scrollPane = new JScrollPane(table);
	    scrollPane.setBounds(6, 53, 454, 279);
	    add(scrollPane);
	    setSize(620, 432);
	    
	    viewLabel = new JLabel(vName);
	    viewLabel.setFont(new Font("Lucida Grande", Font.BOLD, 13));
	    viewLabel.setBounds(6, 24, 284, 17);
	    add(viewLabel);
	    setVisible(true);
	    
	    this.controller = new CasesController(this, modelFilter);
	    table.addMouseListener(this);
	}
	
	public void setTitle(String newTitle) {
		this.viewLabel.setText(newTitle);
		System.out.println("Text was set.");
	}
	
	@SuppressWarnings("unchecked")
	public <E> void update(SimpleObservableArrayList<E> a, Object arg) {

		SimpleObservableArrayList<Case> cases = (SimpleObservableArrayList<Case>) a;
		this.modelArray = cases;
		for( Case c : cases ) {
			tableModel.addRow(new Object[]{c.getId(),c.getAuthor()});
		}	

		
		this.modelArray = (SimpleObservableArrayList<Case>) a;
		this.setRows();
		
		System.out.println("CasesView ("+ viewLabel.getText() + "): update was called");
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent event) {
		// TODO Auto-generated method stub
		int row = table.rowAtPoint(event.getPoint());
		Case caseAtRow = modelArray.get(row);
		controller.openEditCase(caseAtRow);
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
