package caseManagement;


import java.awt.event.ActionListener;


public class CaseController implements ActionListener {

    private Case model;
    private EditCaseView view;

    public CaseController() {
    }

    @Override
    public void actionPerformed(java.awt.event.ActionEvent e) {

        System.out.println("Controller: The " + e.getActionCommand()
                + " button is clicked at " + new java.util.Date(e.getWhen())
                + " with e.paramString " + e.paramString());

        System.out.println("Controller: acting on Model");
        if(Application.getActiveUser().getRole().equals("Manager"))
            model.SaveManagerInput(view.getRadioButtonStatus());
        if(Application.getActiveUser().getRole().equals("Officer"))
            model.SaveOfficerInput(view.getChoosenTechnician(), view.getCostInput(), view.getTimeInput());
    } //actionPerformed()
    
    public void addModel(Case m){
		System.out.println("CaseController: adding model");
		this.model = m;
	} //addModel()

	public void addView(EditCaseView v){
		System.out.println("CaseController: adding view");
		this.view = v;
	} //addView()
}
