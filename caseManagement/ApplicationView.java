package caseManagement;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.CardLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import databaseRepository.SQLValuePair;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ApplicationView implements Observer, ActionListener {

	private JFrame frame;
	JPanel viewCards;
	LoginPanel loginView = new LoginPanel();
	CasesView casesView = new CasesView("All Cases", null);
	final static String LOGINPANEL = "The login view";
	final static String CASESPANEL = "The cases panel";
	
	private ApplicationController controller;
	private Application model;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ApplicationView window = new ApplicationView();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ApplicationView() {
		this.model =  new Application();
		this.controller = new ApplicationController(this, model);
		
		model.addObserver(this);
		// createView is called from the model.
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private JButton logoutButton;
	
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 618, 470);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(6, 6, 604, 54);
		frame.getContentPane().add(panel);
		panel.setLayout(null);

		logoutButton = new JButton("Logout");
		logoutButton.setBounds(527, 12, 71, 29);
		panel.add(logoutButton);
		
		JLabel userLabel = new JLabel();
		userLabel.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		userLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		userLabel.setBounds(316, 18, 213, 16);
		panel.add(userLabel);
		
		JLabel titleLabel = new JLabel("Case Management System");
		titleLabel.setFont(new Font("Lucida Grande", Font.BOLD, 15));
		titleLabel.setBounds(18, 0, 317, 54);
		panel.add(titleLabel);
		
		JPanel viewCards = new JPanel();
		viewCards.setLayout(new CardLayout(0,0));
		viewCards.setBounds(6, 64, 606, 378);
		frame.getContentPane().add(viewCards);

		viewCards.add(loginView, LOGINPANEL);
		viewCards.add(casesView, CASESPANEL);
		
		logoutButton.addActionListener(this);
		loginView.loginButton.addActionListener(this);
		
		this.disableLogoutButton();
		this.loginView.setVisible(true);
		this.casesView.setVisible(false);
	} 

	@Override
	public void update(Observable arg0, Object arg1) {
		System.out.println("ApplicationView: update");
		
		
		if(this.model.userIsLoggedIn()) {
			this.enableLogoutButton();
			this.loginView.setVisible(false);
			this.casesView.setVisible(true);
                        
			ArrayList<SQLValuePair<String, ?>> modelFilter = new ArrayList<SQLValuePair<String, ?>>();
                        modelFilter.add(new SQLValuePair<String,String> ("department", Application.getActiveUser().getDepartment()));
                        if (Application.getActiveUser().getRole().equals("Officer")) {
                            modelFilter.add(new SQLValuePair<String, Integer>("stage", 1));
                        } else if (Application.getActiveUser().getRole().equals("Manager")) {
                            modelFilter.add(new SQLValuePair<String, Integer>("stage", 3));
                        }
			this.casesView.setTitle("Hello " + Application.getActiveUser().getFullName() + 
                                ", theese cases needs to be handled by you.");
			this.casesView.controller.setNewModelFilter(modelFilter);
		}
		
		if(!this.model.userIsLoggedIn()) {
			this.disableLogoutButton();
			this.loginView.setVisible(true);
			this.casesView.setVisible(false);
		}
		
	}
	private void enableLogoutButton(){
		this.logoutButton.setEnabled(true);
	}
	private void disableLogoutButton(){
		this.logoutButton.setEnabled(false);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource() == this.logoutButton) {	
			this.controller.logout();	
		}
		if(e.getSource() == this.loginView.loginButton) {
			this.controller.login(loginView.usernameField.getText(), loginView.passwordField.getText());
			
		}
		
	}

}
