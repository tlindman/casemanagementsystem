package caseManagement;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import databaseRepository.CaseRepository;
import databaseRepository.SQLValuePair;

public class CasesController implements Observer {

    // VIEW AND MODEL
    @SuppressWarnings("unused")
    private CasesView view;
    private SimpleObservableArrayList<Case> model;
    // REPOSITORY ACCESS AND "FILTER"
    private CaseRepository caseRepository;
    private ArrayList<SQLValuePair<String, ?>> modelFilter;

    // CONSTRUCTOR
    public CasesController(CasesView view, ArrayList<SQLValuePair<String, ?>> modelFilter) {
        System.out.println("CasesController is instanciating...");

        this.model = new SimpleObservableArrayList<Case>();
        this.view = view;

        model.addObserver(view);

        this.caseRepository = new CaseRepository();
        this.modelFilter = modelFilter;

        this.reloadModel();

        System.out.println("CasesController was instanciated");
    }

    private void reloadModel() {
        // Empties the modelArray and gets fresh data from the repository based on the initial query / filter.
        model.clear();
        System.out.println("här kallas det på reloadModel");
        if (modelFilter == null) {
            //Här gäller det att hämta hem rätt cases beroende på vem som är inloggad
            //I nuläget funkar det här inte, detta kallas på innan nån är inloggad
            /*
            ArrayList<SQLValuePair<String, ?>> villkor = new ArrayList<SQLValuePair<String, ?>>();
            villkor.add(new SQLValuePair<String, String>("department", Application.getActiveUser().getDepartment()));
            if (Application.getActiveUser().getRole().equals("Officer")) {
                villkor.add(new SQLValuePair<String, Integer>("stage", 1));
            } else if (Application.getActiveUser().getRole().equals("Manager")) {
                villkor.add(new SQLValuePair<String, Integer>("stage", 3));
            }
            model.addAll(caseRepository.FindMany(villkor));
            //model.addAll(caseRepository.);
            */
            model.addAll(caseRepository.FindAll());
        } else {
            model.addAll(caseRepository.FindMany(modelFilter));
        }

        for (Case c : model) {
            c.addObserver(this);
        }
    }
    
	public void setNewModelFilter(ArrayList<SQLValuePair<String, ?>> modelFilter) {
		this.modelFilter = modelFilter;
		this.reloadModel();
	}

    public void addCase(Case aCase) {
        // TODO: (IF REQUIRED) Save aCase to repository and refresh controller
    }

    public void openEditCase(Case aCase) {

        EditCaseView editView = new EditCaseView(new javax.swing.JFrame(), true, aCase);
        aCase.addObserver(editView);

        CaseController editController = new CaseController();

        editController.addModel(aCase);
        editController.addView(editView);
        editView.addController(editController);
    }

    public void openNewCase(Case aCase) {
        // TODO: (IF REQUIRED) Open newCaseView
    }

    public void openShowCase(Case aCase) {
        // TODO: (IF REQUIRED) Open showCaseView
    }

    @Override
    public void update(Observable o, Object arg) {
        System.out.println("CasesController: updates due to change to one or more of the cases");
        this.reloadModel();
    }
}
