package caseManagement;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class LoginPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1345402765171269946L;
	JTextField usernameField;
	JTextField passwordField;
	JButton loginButton;
	JLabel lblUsername;
	JLabel lblPassword;
	
	public LoginPanel() {
		setLayout(null);
		usernameField = new JTextField();
		usernameField.setBounds(120, 86, 134, 28);
		add(usernameField);
		usernameField.setColumns(10);
		
		passwordField = new JTextField();
		passwordField.setBounds(120, 126, 134, 28);
		add(passwordField);
		
		lblUsername = new JLabel("Username");
		lblUsername.setBounds(21, 92, 86, 16);
		add(lblUsername);
		
		lblPassword = new JLabel("Password");
		lblPassword.setBounds(21, 132, 76, 16);
		add(lblPassword);
		
		loginButton = new JButton("Login");
		loginButton.setBounds(137, 166, 117, 29);
		add(loginButton);
		System.out.println("LoginPanel was created");
	}
	
	public String getUsername() {
		return this.usernameField.getText();
	}
	public String getPassword() {
		return this.passwordField.getText();
	}

}
