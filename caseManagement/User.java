package caseManagement;

import java.util.ArrayList;
import databaseRepository.SQLValuePair;

public class User extends Model{
	
	private String firstName;
	private String lastName;
	private String username;
	private String password;
        private String role;
        private String department;
	
	public User(String firstName, String lastName, String username, String password, String role, String department){
		this.firstName = firstName;
		this.lastName = lastName;
		this.username = username;
		this.password = password;
                this.role = role;
                this.department = department;
	}
	@SuppressWarnings({ "rawtypes"})
	public ArrayList<SQLValuePair> getProperties() {
		ArrayList<SQLValuePair> properties = new ArrayList<SQLValuePair>();
		properties.add(new SQLValuePair<String, String>("firstName", this.firstName));
		properties.add(new SQLValuePair<String, String>("lastName", this.lastName));
		properties.add(new SQLValuePair<String, String>("username", this.username));
		properties.add(new SQLValuePair<String, String>("password", this.password));
                properties.add(new SQLValuePair<String, String>("department", this.department));
		return properties;
	}
        
    public String getRole()
    {
        return this.role;
    }
    
    public String getFullName()
    {
        return this.firstName + " " + this.lastName;
    }
    
    public String getUsername()
    {
        return this.username;
    }
    
    public String getDepartment() {
        return this.department;
    }
}
