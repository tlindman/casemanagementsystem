package caseManagement;

public interface SimpleArrayListObserver{
	public <E> void update(SimpleObservableArrayList<E> a, Object arg);
}
