package oldFilesNotUsed;

import databaseRepository.CaseRepository;
import databaseRepository.SQLValuePair;
import java.util.ArrayList;

import caseManagement.Case;
import caseManagement.CaseController;
import caseManagement.EditCaseView;

/**
 *
 * @author Tommy
 */
public class TestRunCaseMVC {

    public static void main(String[] args) {
        CaseRepository repo = new CaseRepository();
        @SuppressWarnings("rawtypes")
	ArrayList<SQLValuePair> villkor = new ArrayList<SQLValuePair>();
        villkor.add(new SQLValuePair<String, Integer>("id", 3));
        Case aCase = repo.FindOne(villkor);
        
        Case myModel = aCase;//new Case("111", "222", "333", true, "555");
        
        EditCaseView myView = new EditCaseView(new javax.swing.JFrame(), true, myModel);

        //tell Model about View. 
        myModel.addObserver(myView);
        
        //create CaseController. tell it about Model and View
        CaseController myController = new CaseController();
        myController.addModel(myModel);
        myController.addView(myView);

        //tell View about Controller 
        myView.addController(myController);

    }
}
