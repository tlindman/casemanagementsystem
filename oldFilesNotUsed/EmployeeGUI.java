package oldFilesNotUsed;

import caseManagement.Case;
import caseManagement.User;
import oldFilesNotUsed.MainFrameGUI;
import oldFilesNotUsed.OngoingCasesPanel;
import oldFilesNotUsed.RegCasePanel;
import databaseRepository.CaseRepository;
import databaseRepository.SQLValuePair;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Tommy
 */
public class EmployeeGUI extends MainFrameGUI {
    
    public EmployeeGUI(User user) throws SQLException{
        super(user.getFullName());
        addTabToMainFrame("Case registration", new RegCasePanel());
        
        CaseRepository caseRepo = new CaseRepository();
        /*
         * Here we create a case based on user information and sends that case
         * to the tab OngoingCasePanel
         */
        ArrayList<SQLValuePair> villkor2 = new ArrayList<SQLValuePair>();
	villkor2.add(new SQLValuePair<String, String>("author", user.getUsername()));
        Case aCase = caseRepo.FindOne(villkor2);
        addTabToMainFrame("Ongoing Cases", new OngoingCasesPanel(aCase));
    }
}
