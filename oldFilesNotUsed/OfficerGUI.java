/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package oldFilesNotUsed;

import caseManagement.User;
import java.sql.SQLException;

/**
 *
 * @author Tommy
 */
public class OfficerGUI extends EmployeeGUI {
    
    public OfficerGUI(User user) throws SQLException {
        super(user);
        addTabToMainFrame("Cases to handle", new HandleCasesPanel());
    }
}
