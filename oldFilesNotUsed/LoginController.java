/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package oldFilesNotUsed;

import java.sql.SQLException;

import caseManagement.User;

/**
 *
 * @author Tommy
 */
public class LoginController {
    
    public static void LoadGUI(User loggedInPerson) throws SQLException
    {
        switch (loggedInPerson.getRole()) {
            case "Employee":
                new EmployeeGUI(loggedInPerson).setVisible(true);
                break;
            case "Officer":
                new OfficerGUI(loggedInPerson).setVisible(true);
                break;
            case "Manager":
                new ManagerGUI(loggedInPerson).setVisible(true);
                break;
        }
    }
}
