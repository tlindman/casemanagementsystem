/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package oldFilesNotUsed;

import caseManagement.User;
import java.sql.SQLException;

/**
 *
 * @author Tommy
 */
public class ManagerGUI extends EmployeeGUI {
    
    public ManagerGUI(User user) throws SQLException {
        super(user);
        addTabToMainFrame("Cases to attest", new AttestPanel());
    }
}
