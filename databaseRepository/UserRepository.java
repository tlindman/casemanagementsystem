package databaseRepository;

import caseManagement.User;

public class UserRepository extends ModelRepository<User>{
	
	public UserRepository() {
		this.tableName = "users";
		this.converter = new ConvertResultToUser();
	}
}
