package databaseRepository;

import java.sql.ResultSet;
import java.sql.SQLException;

import caseManagement.Model;

public interface Converter {
	public Model convert(ResultSet rs) throws SQLException ;
}
