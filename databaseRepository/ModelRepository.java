package databaseRepository;

import java.sql.*;
import java.util.ArrayList;
import caseManagement.Model;

public abstract class ModelRepository <ModelType>{
	
	protected String tableName;
	protected Converter converter;
	
	@SuppressWarnings("unused")
	private DBConnector dbConnector = new DBConnector();
	
	@SuppressWarnings({ "unchecked"})
	private ArrayList<ModelType> execute(String query) throws SQLException 
	{
        /*
         * the callback is a ResultConverter that is called when a ResultSet is received from database.
         * When instantiating a new DBQuery the <ResultType> must be set to the same as the return type of callback.convert();
         */
		Connection con = null;
		Statement stmt;
		ResultSet rs;
		ArrayList<Model> models = new ArrayList<Model>();
		
		try {
			con = DBConnector.getConnection();
	        stmt = con.createStatement();
	        rs = stmt.executeQuery(query);
	        
	        while(rs.next()) {
	        	models.add(this.converter.convert(rs));
	        }
	        rs.close();
	        
	        if(models.isEmpty()) {
				System.out.println("No results found in database");
			}
	        return (ArrayList<ModelType>) models;
	        
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			con.close();
		} 
		return (ArrayList<ModelType>) models;
	}
	
	public ModelType FindOne(ArrayList<SQLValuePair<String,?>> keyValuePairs) {
		
		String query = "SELECT * FROM " + tableName + " WHERE ";
		
		for(int i = 0; i < keyValuePairs.size(); i++) 
		{
			query = query + keyValuePairs.get(i).getKey() + "=" + keyValuePairs.get(i).getSqlValue();
			
			if(i < keyValuePairs.size() - 1) {
				query = query + " AND ";
			}
		}	
		
		try {
			ArrayList<ModelType> records = this.execute(query);
			if(records.size() > 1) {
				System.out.println("More than one records was found, returning the first one..");
			}
			if(records.isEmpty()) {
				return null;
			}
			return records.get(0);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public ArrayList<ModelType> FindMany(ArrayList<SQLValuePair<String, ?>> keyValuePairs) {
	String query = "SELECT * FROM " + tableName + " WHERE ";
		
		for(int i = 0; i < keyValuePairs.size(); i++) 
		{
			query = query + keyValuePairs.get(i).getKey() + "=" + keyValuePairs.get(i).getSqlValue();
			
			if(i < keyValuePairs.size() - 1) {
				query = query + " AND ";
			}
		}		
		
		try {
			return this.execute(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public ArrayList<ModelType> FindAll() {
		String query = "SELECT * FROM " + tableName;

		try {
			return this.execute(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void Save(Model model) {
		String query;
		Connection con = null;
		Statement stmt;
		
		if(model.getId() < 1) {
			query = "INSERT INTO " + tableName;
			String columns = " ("; 
			String values = " VALUES(";
			
			for(int i = 0; i < model.getProperties().size(); i++) 
				{
					columns = columns + model.getProperties().get(i).getKey();
					values = values + model.getProperties().get(i).getSqlValue();
						
					if(i < model.getProperties().size() - 1) {
						columns = columns + ",";
						values = values + ",";
					}
				}	
				query = query + columns + ")" + values + ")";	
					
		} else {
			query = "UPDATE " + tableName + " SET ";
				
			for(int i = 0; i < model.getProperties().size(); i++) 
			{
				query = query + model.getProperties().get(i).getKey() + "=" +  model.getProperties().get(i).getSqlValue(); 
					
				if(i < model.getProperties().size() - 1) {
					query = query + ", ";
				}
			}	
			query = query + " WHERE id=" + model.getId();
		}
				
		try {
			con = DBConnector.getConnection();
		    stmt = con.createStatement();
		    stmt.executeUpdate(query);
			        
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} 
	}
}
