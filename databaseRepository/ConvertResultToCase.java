package databaseRepository;

import java.sql.ResultSet;
import java.sql.SQLException;

import caseManagement.Case;
import caseManagement.Model;

public class ConvertResultToCase implements Converter {

	Case aCase;
	@Override
	public Model convert(ResultSet rs) throws SQLException {
		 String description = rs.getString("description");
         String report = rs.getString("report");
         String totalCost = rs.getString("totalcost");
         String totalHours = rs.getString("totalhours");
         String actualCost = rs.getString("actualcost");
         String actualHours = rs.getString("actualhours");
         boolean isCostApproved = rs.getBoolean("iscostapproved");
         int stage = rs.getInt("stage");
         String author = rs.getString("author");
         String assignedTechnician = rs.getString("assignedTechnician");
         aCase = new Case(description, report, totalCost, totalHours, actualCost, actualHours, isCostApproved, stage, author, assignedTechnician);
         aCase.setId(rs.getInt("id"));
         return aCase;
	}

}
