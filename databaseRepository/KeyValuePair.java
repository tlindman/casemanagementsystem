package databaseRepository;

public interface KeyValuePair<K, V> {
	public K getKey();
	public V getValue();
}
