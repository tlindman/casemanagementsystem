package databaseRepository;

import java.sql.ResultSet;
import java.sql.SQLException;

import caseManagement.Model;
import caseManagement.User;

public class ConvertResultToUser implements Converter{
	User user;
	@Override
	public Model convert(ResultSet rs) throws SQLException {
		String firstname = rs.getString("firstname");
    	String lastname = rs.getString("lastname");
    	String username = rs.getString("username");
    	String password = rs.getString("password"); 
    	String role = rs.getString("role");
        String department = rs.getString("department");
    	user = new User(firstname, lastname, username, password, role, department);
    	user.setId(rs.getInt("id"));
    	return user;
	}
}
