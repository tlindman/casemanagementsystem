package databaseRepository;

import caseManagement.Case;

public class CaseRepository extends ModelRepository<Case> {
	
	public CaseRepository() {
		this.tableName = "cases";
		this.converter = new ConvertResultToCase();
	}
}
