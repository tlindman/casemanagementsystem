package databaseRepository;

public class SQLValuePair<K, V> implements KeyValuePair<K, V> {
    /*
     * When this one is implemented you must specify types, for example new KeyValuePair<String, Integer>
     */

    private K key;
    private V value = null;

    public SQLValuePair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }

    public String getSqlValue() {
        /*
         * The purpose of this method is to return SQL adapted values for int, bool and string
         */
        if (value != null) {

            String str = String.valueOf(value);

            try {
                Boolean bool = (Boolean) value;
                return String.valueOf(bool).toUpperCase();

            } catch (ClassCastException e) {
                try {
                    Integer.parseInt(str);  // Just checking if the value is of integer type.. not used for anything else.
                    return str;

                } catch (NumberFormatException e2) {
                    return "'" + str + "'";
                }
            }
        } else {
            return null; // Should return "IS NULL"
        }
    }
}